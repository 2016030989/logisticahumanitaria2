package com.android.logisticahumanitaria2;

import java.io.Serializable;

public class Estado implements Serializable {

    private String idMovil;
    private int _ID;
    private String nombre;
    private int favorito;
    private String notas;

    public Estado() {
        this._ID = 0;
        this.nombre = "";
        this.favorito = 0;
        this.notas = "";
        this.idMovil = "";
    }

    public Estado(int _ID, String nombre, int favorito, String notas, String idMovil) {
        this._ID = _ID;
        this.nombre = nombre;
        this.idMovil = idMovil;
        this.favorito = favorito;
        this.notas = notas;
    }

    public Estado(Estado estado) {
        this._ID = estado.get_ID();
        this.nombre = estado.getNombre();
        this.favorito = estado.getFavorito();
        this.notas = estado.getNotas();
    }

    public String getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(String idMovil) {
        this.idMovil = idMovil;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }
}
