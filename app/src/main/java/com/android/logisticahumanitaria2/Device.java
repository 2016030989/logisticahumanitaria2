package com.android.logisticahumanitaria2;

import android.content.Context;
import android.provider.Settings;

public class Device {
    private String idMovil;

    public Device(Context context) {
        this.idMovil = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
    }

    public String getIdMovil() {
        return idMovil;
    }
}
