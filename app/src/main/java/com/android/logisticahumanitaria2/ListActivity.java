package com.android.logisticahumanitaria2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity implements Response.ErrorListener, Response.Listener<JSONObject> {
    private DbEstados dbEstados;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue requestQueue;
    private ArrayList<Estado> estados;
    private EstadoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        requestQueue = Volley.newRequestQueue(this);

        Button btnNuevo = (Button)findViewById(R.id.btnNuevoEstado);

        dbEstados = new DbEstados(this);
        dbEstados.openDatabase();

        Device device = new Device(this);

        estados = new ArrayList<Estado>();
        consultarTodos(device.getIdMovil());

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public void consultarTodos(String idMovil) {
        String url = "https://estadosapp.000webhostapp.com/wsVerEstados.php?" +
                "idMovil=" + idMovil;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this,"ERROR DE RED: " + error.getMessage(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.optJSONArray("estados");
            JSONObject jsonObject = null;

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                Estado estado1 = new Estado();
                estado1.set_ID(jsonObject.getInt("idEstados"));
                estado1.setFavorito(jsonObject.getInt("favorito"));
                estado1.setIdMovil(jsonObject.getString("idMovil"));
                estado1.setNombre(jsonObject.getString("nombre"));
                estado1.setNotas(jsonObject.getString("notas"));
                estados.add(estado1);
            }
            adapter = new EstadoAdapter(this,R.layout.item_contacto, estados);
            setListAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    class EstadoAdapter extends ArrayAdapter<Estado> implements Response.ErrorListener, Response.Listener<JSONObject> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Estado> objects;

        public EstadoAdapter(Context context, int textViewResourceId,ArrayList<Estado> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView tVNombre = (TextView) view.findViewById(R.id.tVNombreItem);
            Button btnModificar = (Button) view.findViewById(R.id.btnModiItem);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrarItem);

            if(objects.get(position).getFavorito()>0){
                tVNombre.setTextColor(Color.BLUE);
            }else{
                tVNombre.setTextColor(Color.BLACK);
            }

            tVNombre.setText(objects.get(position).getNombre());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbEstados.openDatabase();
                    dbEstados.deleteEstado(objects.get(position).get_ID());
                    dbEstados.close();
                    wsEliminar(objects.get(position).get_ID());
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estado", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }

        public void wsEliminar(int idEstados) {
            //Este método deshabilita los estados (con un status) del servidor. El estado es el del
            // id que se recibe como parametro.
            String url = "https://estadosapp.000webhostapp.com/wsEliminar.php?" +
                    "idEstados=" + idEstados;

            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            requestQueue.add(jsonObjectRequest);
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(JSONObject response) {

        }
    }
}
