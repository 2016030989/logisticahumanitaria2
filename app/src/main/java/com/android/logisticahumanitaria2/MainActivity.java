package com.android.logisticahumanitaria2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject> {
    private EditText eTNombre;
    private EditText eTNota;
    private CheckBox cBFav;
    private Button btnLimpiar;
    private Button btnGuardar;
    private Button btnVerEstados;
    private Estado saverEstado;
    private int id;
    private String wsElegido;

    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(this);

        eTNombre = (EditText) findViewById(R.id.eTNombreEstado);
        eTNota = (EditText) findViewById(R.id.eTNotas);
        cBFav = (CheckBox) findViewById(R.id.cbFav);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnVerEstados = (Button) findViewById(R.id.btnVerEstados);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    // Si hay conexión a Internet en este momento
                    boolean puedeContinuar = true;

                    if(eTNombre.getText().toString().equals("")) {
                        eTNombre.setError("Ingresa el nombre del estado");
                        puedeContinuar = false;
                    }

                    if(puedeContinuar) {
                        DbEstados source = new DbEstados(MainActivity.this);
                        source.openDatabase();
                        Estado nEstado = new Estado();
                        nEstado.setNombre(eTNombre.getText().toString());
                        nEstado.setNotas(eTNota.getText().toString());
                        nEstado.setFavorito(cBFav.isChecked() ? 1 : 0);
                        if (saverEstado == null) {
                            source.insertEstado(nEstado);
                            wsElegido = "wsGuardar";
                            Device device = new Device(MainActivity.this);
                            wsGuardar(eTNombre.getText().toString(),cBFav.isChecked() ? 1 : 0,eTNota.getText().toString(),device.getIdMovil());
                            Toast.makeText(MainActivity.this, "Estado guardado!",
                                    Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            source.updateEstado(nEstado, id);
                            wsActualizar(eTNombre.getText().toString(),cBFav.isChecked() ? 1 : 0,eTNota.getText().toString(),id);
                            Toast.makeText(MainActivity.this, "Estado actualizado!",
                                    Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        source.close();
                    }
                } else {
                    // No hay conexión a Internet en este momento
                    Toast.makeText(MainActivity.this,"SIN CONEXION A INTERNET",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnVerEstados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,ListActivity.class);
                startActivityForResult(i, 0);
            }
        });

        agregarLos10Estados();
        wsVerificarConexion();
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estado estado = (Estado) data.getSerializableExtra("estado");
            saverEstado = estado;
            id = ((int) estado.get_ID());
            eTNombre.setText(estado.getNombre());
            eTNota.setText(estado.getNotas());
            if (estado.getFavorito() > 0) {
                cBFav.setChecked(true);
            }
        }else{
            limpiar();
        }
    }

    public void limpiar(){
        saverEstado = null;
        eTNombre.setText("");
        eTNota.setText("");
        cBFav.setChecked(false);
    }

    public void agregarLos10Estados() {
        DbEstados source = new DbEstados(MainActivity.this);
        source.openDatabase();
        source.eliminarLos10Estados();

        Estado nEstado1 = new Estado();
        nEstado1.setNombre("Veracruz");
        nEstado1.setNotas("Estado autogenerado");
        nEstado1.setFavorito(1);
        source.insertEstado(nEstado1);

        Estado nEstado2 = new Estado();
        nEstado2.setNombre("Sonora");
        nEstado2.setNotas("Estado autogenerado");
        nEstado2.setFavorito(1);
        source.insertEstado(nEstado2);

        Estado nEstado3 = new Estado();
        nEstado3.setNombre("Chihuahua");
        nEstado3.setNotas("Estado autogenerado");
        nEstado3.setFavorito(1);
        source.insertEstado(nEstado3);

        Estado nEstado4 = new Estado();
        nEstado4.setNombre("Nayarit");
        nEstado4.setNotas("Estado autogenerado");
        nEstado4.setFavorito(1);
        source.insertEstado(nEstado4);

        Estado nEstado5 = new Estado();
        nEstado5.setNombre("Sinaloa");
        nEstado5.setNotas("Estado autogenerado");
        nEstado5.setFavorito(1);
        source.insertEstado(nEstado5);

        Estado nEstado6 = new Estado();
        nEstado6.setNombre("Yucatan");
        nEstado6.setNotas("Estado autogenerado");
        nEstado6.setFavorito(1);
        source.insertEstado(nEstado6);

        Estado nEstado7 = new Estado();
        nEstado7.setNombre("Baja California");
        nEstado7.setNotas("Estado autogenerado");
        nEstado7.setFavorito(1);
        source.insertEstado(nEstado7);

        Estado nEstado8 = new Estado();
        nEstado8.setNombre("Michoacan");
        nEstado8.setNotas("Estado autogenerado");
        nEstado8.setFavorito(1);
        source.insertEstado(nEstado8);

        Estado nEstado9 = new Estado();
        nEstado9.setNombre("Jalisco");
        nEstado9.setNotas("Estado autogenerado");
        nEstado9.setFavorito(1);
        source.insertEstado(nEstado9);

        Estado nEstado10 = new Estado();
        nEstado10.setNombre("Durango");
        nEstado10.setNotas("Estado autogenerado");
        nEstado10.setFavorito(1);
        source.insertEstado(nEstado10);

        source.close();
    }

    public void wsVerificarConexion() {
        String url = "https://estadosapp.000webhostapp.com/wsJSONCargarEstadoDelMantenimiento.php";

        wsElegido = "wsVerificarConexion";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    public void wsActualizar(String nombre, int fav, String nota, int idEstados) {
        String url = "https://estadosapp.000webhostapp.com/wsModificar.php?" +
                "nombre=" + nombre +
                "&favorito=" + fav +
                "&notas=" + nota +
                "&idEstados=" + idEstados;

        url = url.replace(" ","%20");
        wsElegido = "";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    public void wsGuardar(String nombre, int fav, String nota, String idMovil) {
        String url = "https://estadosapp.000webhostapp.com/wsAgregar.php?" +
                "nombre=" + nombre +
                "&favorito=" + fav +
                "&notas=" + nota +
                "&idMovil=" + idMovil;

        url = url.replace(" ","%20");
        wsElegido = ""; 
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this,"SIN CONEXION A INTERNET",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        if(wsElegido.equals("wsVerificarConexion")) {
            Toast.makeText(this,"CONEXION A INTERNET ESTABLECIDA",Toast.LENGTH_SHORT).show();

            SharedPreferences preferencias = getSharedPreferences("Movil", this.MODE_PRIVATE);
            int recuerdaSesion = preferencias.getInt("primeraVez",1);

            if(recuerdaSesion==1) {
                //es la primera vez que inicia la app.
                Toast.makeText(MainActivity.this,"Bienvenido por primera vez!",Toast.LENGTH_SHORT).show();
                wsCargarEstados();
            }

            SharedPreferences.Editor editor = preferencias.edit();
            editor.putInt("primeraVez",0);
            editor.apply();
            editor.commit();
        }else if(wsElegido.equals("wsCargarEstados")) {
            try{
                JSONArray jsonArray = response.optJSONArray("estados");
                JSONObject jsonObject = null;

                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDatabase();
                    Estado estado = new Estado();
                    estado.setNombre(jsonObject.optString("nombre"));
                    estado.setNotas(jsonObject.optString("notas"));
                    estado.setFavorito(jsonObject.optInt("favorito"));
                    estado.setIdMovil(jsonObject.optString("idMovil"));
                    estado.set_ID(jsonObject.optInt("idEstados"));
                    source.insertEstado(estado);
                    source.close();
                }
                }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void wsCargarEstados() {
        Device device = new Device(this);
        String url = "https://estadosapp.000webhostapp.com/wsVerEstados.php?" +
                "idMovil=" + device.getIdMovil();

        wsElegido = "wsCargarEstados";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }
}