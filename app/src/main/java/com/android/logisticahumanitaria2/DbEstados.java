package com.android.logisticahumanitaria2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {
    private Context context;
    private DbHelper mDbHelper;
    private SQLiteDatabase db;
    private String[] columnsToRead = new String[] {
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE,
            DefinirTabla.Estado.COLUMN_NAME_NOTAS,
            DefinirTabla.Estado.COLUMN_NAME_FAVORITO
    };

    public DbEstados(Context context){
        this.context = context;
        mDbHelper = new DbHelper(this.context);
    }
    public void openDatabase(){
        db = mDbHelper.getWritableDatabase();
    }
    public long insertEstado(Estado e){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, e.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOTAS, e.getNotas());
        values.put(DefinirTabla.Estado.COLUMN_NAME_FAVORITO, e.getFavorito());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
        //regresa el id insertado
    }
    public long updateEstado(Estado e, int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, e.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOTAS, e.getNotas());
        values.put(DefinirTabla.Estado.COLUMN_NAME_FAVORITO, e.getFavorito());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }
    public int deleteEstado(long id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME,DefinirTabla.Estado._ID + "=?", new String[]{ String.valueOf(id) });
    }

    private Estado readEstado(Cursor cursor){
        Estado e = new Estado();
        e.set_ID(cursor.getInt(0));
        e.setNombre(cursor.getString(1));
        e.setNotas(cursor.getString(2));
        e.setFavorito(cursor.getInt(3));
        return e;
    }

    public Estado getEstado(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnsToRead,
                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estado estado = readEstado(c);
        c.close();
        return estado;
    }

    public ArrayList<Estado> allEstados() {
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnsToRead, null, null, null, null, null);
        ArrayList<Estado> contactos = new ArrayList<Estado>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Estado e = readEstado(cursor);
            contactos.add(e);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }


    public void eliminarLos10Estados() {
        db.execSQL("DELETE FROM estados WHERE " +
                "nombre='Veracruz' " +
                "OR nombre='Sinaloa'" +
                "OR nombre='Sonora'" +
                "OR nombre='Chihuahua'" +
                "OR nombre='Nayarit' " +
                "OR nombre='Yucatan'" +
                "OR nombre='Baja California'" +
                "OR nombre='Michoacan'" +
                "OR nombre='Jalisco'" +
                "OR nombre='Durango'");
    }


    public void close(){
        mDbHelper.close();
    }
}
