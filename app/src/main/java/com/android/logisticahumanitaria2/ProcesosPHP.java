package com.android.logisticahumanitaria2;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProcesosPHP implements Response.ErrorListener, Response.Listener<JSONObject> {
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue requestQueue;
    private Context context;

    public ProcesosPHP(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public void insert(String nombre, String nota, int favorito, String idMovil) {
        String url = "https://estadosapp.000webhostapp.com/wsAgregar.php?" +
                "nombre=" + nombre +
                "&favorito=" + favorito +
                "&notas=" + nota +
                "&idMovil=" + idMovil;
        url = url.replace(" ","%20");

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    public void actualizar(String nombre, int favorito, String notas, int idEstados) {
        String url = "https://estadosapp.000webhostapp.com/wsModificar.php?" +
                "nombre=" + nombre +
                "&favorito=" + favorito +
                "&notas=" + notas +
                "&idEstados=" + idEstados;
        url = url.replace(" ","%20");

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    public void borrar(int idEstados) {
        String url = "https://estadosapp.000webhostapp.com/wsEliminar.php?" +
                "idEstados=" + idEstados;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(context,"HECHO!",Toast.LENGTH_SHORT).show();
    }

    public void mostrarContacto(int idEstados) {
        String url = "https://estadosapp.000webhostapp.com/wsVerEstado.php?" +
                "idEstados=" + idEstados;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }
}
